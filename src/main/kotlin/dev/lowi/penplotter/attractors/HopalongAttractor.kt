package dev.lowi.penplotter.attractors

import toxi.geom.Vec2D
import java.util.*
import kotlin.math.abs
import kotlin.math.sign
import kotlin.math.sqrt

class HopalongAttractor(
    private val vector: Vec2D = Vec2D(0f, 0f),
    private val a: Double = 7.16878197155893,
    private val b: Double = 8.43659746693447,
    private val c: Double = 2.55983412731439
) : Attractor {

    override fun draw(steps: Int): List<Vec2D> {
        val vertices = LinkedList<Vec2D>()
        vertices.add(vector)

        return (1..steps).fold(vertices) { acc: LinkedList<Vec2D>, _ ->
            val current: Vec2D = acc.last()
            val newX: Float = (current.y - 1 - sqrt(abs(b * current.x - 1 - c)) * sign(current.x - 1)).toFloat()
            val newY: Float = (a - current.x - 1).toFloat()
            val newVector = Vec2D(newX, newY)
            acc.add(newVector)
            acc
        }
    }
}
