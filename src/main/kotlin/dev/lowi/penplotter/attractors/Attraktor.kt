package dev.lowi.penplotter.attractors

import processing.core.PApplet

class Attraktor : PApplet() {

    companion object {
        fun run() {
            val art = Attraktor()
            art.runSketch()
        }
    }


    var hopalong: HopalongAttractor = HopalongAttractor()

    override fun settings() {
        size(1024, 1024, SVG, "out/attractor.svg")
    }

    override fun setup() {
    }

    override fun draw() {
        noFill()
        smooth()
        background(255)
        translate(width / 2f, height / 2f)
        drawCurve()
        println("Finished.")
        exit()
    }

    private fun drawCurve() {
        val vertices = hopalong.draw(10000)
        stroke(0)
        strokeWeight(0.1f)
        beginShape()
        vertices.iterator()
            .forEach { curveVertex(it.x, it.y) }
        endShape(OPEN)

    }

}

fun main(argh: Array<String>) {
    Attraktor.run()
}

