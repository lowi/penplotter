package dev.lowi.penplotter.attractors

import toxi.geom.Vec2D

interface Attractor {
    fun draw(steps: Int): List<Vec2D>
}