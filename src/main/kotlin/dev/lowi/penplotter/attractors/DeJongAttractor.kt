package dev.lowi.penplotter.attractors

import processing.core.PApplet
import processing.core.PApplet.blendColor
import processing.core.PConstants.RGB
import processing.core.PConstants.SOFT_LIGHT
import processing.core.PImage

class DeJongAttractor(
    private val applet: PApplet,
    private val pa: Float,
    private val pb: Float,
    private val pc: Float,
    private val pd: Float,
) {

    private val N: Int = applet.width
    lateinit var pi: PImage
    var newx: Float = 0f
    var newy: Float = 0f
    var logmaxd: Float = 0f
    var maxdense = 0
    var density = Array(N) { IntArray(N) }
    var previousx = Array(N) { FloatArray(N) }
    var oldx = applet.width / 2f
    var oldy = applet.height / 2f


    fun populate(s: Int, c: Boolean) {
        //Populate array with density info with s number of samples
        if (c) {
            for (i in 0 until N) {
                for (j in 0 until N) {
                    density[i][j] = 0
                    previousx[i][j] = 0f
                }
            }
        }
        for (i in 0 until s) {
            for (j in 0..9999) {
                //De Jong's attractor
                newx = ((PApplet.sin(pa * oldy) - PApplet.cos(pb * oldx)) * N * 0.2f + N / 2)
                newy = ((PApplet.sin(pc * oldx) - PApplet.cos(pd * oldy)) * N * 0.2f + N / 2)
                //Smoothie
                newx += applet.random(-0.001f, 0.001f)
                newy += applet.random(-0.001f, 0.001f)
                //If coordinates are within range, up density count at its position
                //If coordinates are within range, up density count at its position
                if ((newx > 0) && (newx < N) && (newy > 0) && (newy < N) ) {
                    density[newx.toInt()][newy.toInt()] += 1
                    previousx[newx.toInt()][newy.toInt()] = oldx
                }
                oldx = newx
                oldy = newy
            }
        }
        //Put maximum density and its log()-value into variables
        for (i in 0 until N) {
            for (j in 0 until N) {
                if (density[i][j] > maxdense) {
                    maxdense = density[i][j]
                    logmaxd = PApplet.log(maxdense.toFloat())
                }
            }
        }
    }

//    fun updateloop() {
//        stop = false
//        stepCounter = 0
//    }
//
//    fun incrementalupdate() {
//        //Loops the non-clearing update and plotting to produce low-noise render
//        populate(16, false)
//        plot(0, false)
//        redraw()
//    }
//
//    fun reparam() {
//        //Fast reparametrization of variables
//        dj.construct()
//        dj.populate(1, true)
//        dj.plot(100, true)
//    }

    fun plot(f: Int, c: Boolean): PImage {
        //Plot image from density array
        if (c) {
            pi = applet.createImage(N, N, RGB)
        }
        pi.loadPixels()
        for (i in 0 until N) {
            for (j in 0 until N) {
                if (density[i][j] > 0) {
                    val myhue: Float = PApplet.map(
                        previousx[i][j],
                        0f,
                        N.toFloat(),
                        128f,
                        255f
                    ) //Select hue based on the x-coord that gave rise to current coord
                    val mysat: Float = PApplet.map(PApplet.log(density[i][j].toFloat()), 0f, logmaxd, 128f, 0f)
                    val mybright: Float = PApplet.map(PApplet.log(density[i][j].toFloat()), 0f, logmaxd, 0f, 255f) + f
                    var newc = applet.color(myhue, mysat, mybright)
                    val oldc: Int = pi.pixels[i * N + j]
                    newc = blendColor(newc, oldc, SOFT_LIGHT)
                    pi.pixels[i * N + j] = newc
                }
            }
        }
        pi.updatePixels()
        return pi
    }
}