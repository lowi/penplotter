package dev.lowi.penplotter.pixels

import com.curiouscreature.kotlin.math.Float2
import dev.lowi.graphics.Path
import dev.lowi.graphics.PathElement
import toxi.geom.Vec2D

fun squarePixel(center: Float2, scale: Float): List<PathElement> {
    val sx = intArrayOf(-1, +1, +1, -1) // corner directions for x
    val sy = intArrayOf(-1, -1, +1, +1) // corner directions for y
    return sx.zip(sy)
        .map { ( x, y ) -> Float2(x * scale, x * scale) }
        .map { v: Float2 -> center + v }
        .map { v: Float2 -> PathElement.PathPoint(v) }
}

sealed class Pixel(vertices: List<PathElement>, weight: Float = 1f, color: Int = -16777216) : Path(vertices, weight, color) {

    constructor(point: PathElement.PathPoint, color: Int): this(listOf(point), 1f, color)

    class Dot(vertex: PathElement.PathPoint, color: Int): Pixel(vertex, color)

    class Square(center: Float2, size: Float, color: Int): Pixel(squarePixel(center, size), 1f, color)

}