package dev.lowi.penplotter

import dev.lowi.graphics.Path
import dev.lowi.graphics.PathProvider
import toxi.geom.Vec2D
import kotlin.math.*

interface Hilbert

interface HilbertOptions : SketchOptions<Hilbert> {
    val depth: Int
}

data class Corners( val topLeft: Vec2D, val topRight: Vec2D, val bottomRight: Vec2D, val bottomLeft: Vec2D) {
    constructor(corners: List<Vec2D>) : this(
        corners[0], corners[1], corners[2], corners[3]
    )

    fun subdivide(): Corners {
        val topLeft = this.topLeft.scale(0.5f)
        val topRight = this.topRight.scale(0.5f)
        val bottomRight = this.bottomRight.scale(0.5f)
        val bottomLeft = this.bottomLeft.scale(0.5f)
        return Corners(topLeft, topRight, bottomRight, bottomLeft)
    }
}


class HilbertCurve : PathProvider<Hilbert> {

    val DEPTH: Int = 6 // hilbert curve params
    var IDX: Int = 0 // coord index

    val numVerts = 4.0.pow(DEPTH.toDouble()).toInt()
    var verts: Array<Vec2D> = Array(numVerts) { Vec2D(0f, 0f) }

    var SXD: Array<FloatArray> = Array(DEPTH) { FloatArray(4) }
    var SYD: Array<FloatArray> = Array(DEPTH) { FloatArray(4) }

//    val topLeftCorner = Vec2D(-1f, -1f)
//    val topRightCorner = Vec2D(1f, -1f)
//    val bottomRightCorner = Vec2D(1f, 1f)
//    val bottomLeftCorner = Vec2D(-1f, 1f)
//    val corners = Corners(topLeftCorner, topRightCorner, bottomRightCorner, bottomLeftCorner)

    override fun generate(o: SketchOptions<Hilbert>): List<Path> {
        val  options = o as HilbertOptions

        val sx = intArrayOf(-1, +1, +1, -1) // corner directions for x
        val sy = intArrayOf(-1, -1, +1, +1) // corner directions for y
        val corners =  Corners(sx.zip(sy).map { ( x, y ) -> Vec2D(x.toFloat(), y.toFloat()) })

        // precompute quad corners for each depth
        var s = 1.0f
        var i: Int = options.depth - 1

        while (i >= 0) {
            for (j in 0..3) {
                SXD[i][j] = sx[j] * s
                SYD[i][j] = sy[j] * s
            }
            i--
            s /= 2f
        }
//        (options.depth..1).mapIndexed { index, depth ->
//            for (j in 0..3) {
//                SXD[depth][j] = (sx[j] * s / 2.0.pow(index)).toFloat()
//                SYD[depth][j] = (sy[j] * s / 2.0.pow(index)).toFloat()
//            }
//        }


        // create curve
        IDX = 0
        val initialPosition = Vec2D(0f, 0f)
        val vertices = hilbert2D(initialPosition, DEPTH, 0, 1, 2, 3)

        return emptyList()
    }

    /**
     * Hilbert Curve: generates 2D-Coordinates in a very fast way.
     *
     * @param A  corner index A
     * @param B  corner index B
     * @param C  corner index C
     * @param D  corner index D
     */
    private fun hilbert2D(position: Vec2D, depth: Int, A: Int, B: Int, C: Int, D: Int): List<Vec2D> {
        var d = depth
        if (d-- == 0) {
//            verts[IDX][0] = x
//            verts[IDX][1] = y
            IDX++
        } else {
            val X: FloatArray = SXD[d]
            val Y: FloatArray = SYD[d]

            val newPosition = Vec2D()
//            hilbert2D(x + X[A], y + Y[A], d, A, D, C, B)
//            hilbert2D(x + X[B], y + Y[B], d, A, B, C, D)
//            hilbert2D(x + X[C], y + Y[C], d, A, B, C, D)
//            hilbert2D(x + X[D], y + Y[D], d, C, B, A, D)
        }
        return emptyList()
    }

}