package dev.lowi.penplotter

import com.curiouscreature.kotlin.math.Float2
import dev.lowi.graphics.Font
import dev.lowi.graphics.Path
import dev.lowi.graphics.PathElement
import processing.core.PApplet

interface SketchOptions<T>

const val A4_Ratio = 297f / 210
const val SIZE_X = 1024
const val SIZE_Y = (SIZE_X * A4_Ratio).toInt()

class PenplotterApplet : PApplet() {

    companion object {
        fun run() {
            val art = PenplotterApplet()
            art.runSketch()
        }
    }


    override fun settings() {
        size(SIZE_X, SIZE_Y)
        kotlin.io.println("Size: ${SIZE_X}x${SIZE_Y}")
        noLoop()
    }

    override fun setup() {
        beginRecord(SVG, "out/foo.svg")
    }

    override fun draw() {
        noFill()
        smooth()
        background(255)
//        translate(width / 2f, height / 2f)
        drawCurve()
        endRecord()
    }

    private fun drawOutline() {
        val weight = 0.125f
        val halfWeight = weight/2
        val corners = arrayOf(
            Float2(halfWeight, halfWeight),
            Float2(SIZE_X - halfWeight, halfWeight),
            Float2(SIZE_X - halfWeight, SIZE_Y - halfWeight ),
            Float2(halfWeight, SIZE_Y - halfWeight ),
            Float2(halfWeight, halfWeight )
        )
        val color = color(0, 255, 255)
        val outlinePath = Path.fromFloat2(weight, color, *corners)
        drawPath(outlinePath)
    }


    private fun drawCurve(scale: Float = 120f) {

        val digit = Font.Special.Ampersand
        val bb0 = digit.boundingBox
        println("bb0: $bb0")

        drawOutline()

        stroke(0)
        strokeWeight(1f)
        beginShape()
        digit.iterator().forEach {
            when(it) {
                PathElement.Separator -> {
                    endShape(OPEN)
                    beginShape()
                }
                is PathElement.PathPoint -> vertex(it.x * scale, it.y * scale)
            }
        }
        endShape(OPEN)
    }

    private fun drawPath(path: Path) {

        stroke(path.color)
        strokeWeight(8f)
        beginShape()
        path.iterator().forEach {
            when(it) {
                PathElement.Separator -> {
                    endShape(OPEN)
                    beginShape()
                }
                is PathElement.PathPoint -> vertex(it.x , it.y)
            }
        }
        endShape(OPEN)
    }
}


fun main(argh: Array<String>) {
    PenplotterApplet.run()
}

