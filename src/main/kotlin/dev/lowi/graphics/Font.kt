package dev.lowi.graphics

import com.curiouscreature.kotlin.math.Float2
import dev.lowi.graphics.PathElement.PathPoint
import dev.lowi.graphics.PathElement.Separator as S


fun p(x: Int, y: Int): PathPoint = PathPoint(x, y)

fun g(vararg elements: PathElement): List<PathElement> =
    listOf(*elements)
        .map { path -> path.scale(Float2(1 / 12f, 1 / 12f)) } // normalize


sealed class Font(elements: List<PathElement>) : Path(elements) {

    object Digits {
        object Zero : Font(g(p(0, 12), p(8, 12), p(8, 0), p(0, 0), p(0, 12), p(8, 0)))
        object One : Font(g(p(4, 12), p(4, 0), p(3, 2)))
        object Two : Font(g(p(0, 0), p(8, 0), p(8, 5), p(0, 7), p(0, 12), p(8, 12)))
        object Three : Font(g(p(0, 12), p(8, 12), p(8, 0), p(0, 0), S, p(0, 6), p(8, 6)))
        object Four : Font(g(p(0, 0), p(0, 6), p(8, 6), S, p(8, 0), p(8, 12)))
        object Five : Font(g(p(0, 12), p(8, 12), p(8, 6), p(0, 5), p(0, 0), p(8, 0)))
        object Six : Font(g(p(0, 0), p(0, 12), p(8, 12), p(8, 7), p(0, 5)))
        object Seven : Font(g(p(0, 0), p(8, 0), p(4, 12)))
        object Eight : Font(g(p(0, 12), p(8, 12), p(8, 0), p(0, 0), p(0, 12), S, p(0, 6), p(8, 6)))
        object Nine : Font(g(p(8, 12), p(8, 0), p(0, 0), p(0, 5), p(8, 7)))
    }

    object Special {
        object Space  : Font(g(S))
        object Dot : Font(g(p(3, 12), p(4, 12)))
        object Comma : Font(g(p(2, 12), p(4, 10)))
        object Hyphen : Font(g(p(2, 6), p(6, 6)))
        object Plus : Font(g(p(1, 6), p(7, 6), S, p(4, 3), p(4, 9)))
        object Exclamation : Font(g(p(4, 12), p(3, 10), p(5, 10), p(4, 12), S, p(4, 8), p(4, 0)))
        object Hash : Font(g(p(0, 8), p(8, 8), p(6, 10), p(6, 2), p(8, 4), p(0, 4), p(2, 2), p(2, 10)))
        object Caret : Font(g(p(2, 6), p(4, 0), p(6, 6)))
        object Equals : Font(g(p(1, 8), p(7, 8), S, p(1, 4), p(7, 4)))
        object Times : Font(g(p(0, 12), p(4, 0), p(8, 12), p(0, 4), p(8, 4), p(0, 12)))
        object Underscore : Font(g(p(0, 12), p(8, 12)))
        object Slash : Font(g(p(0, 12), p(8, 0)))
        object Backslash : Font(g(p(0, 0), p(8, 12)))
        object At : Font(g(p(8, 8), p(4, 12), p(0, 8), p(0, 4), p(4, 0), p(8, 4), p(4, 8), p(3, 6)))
        object Dollar : Font(g(p(6, 10), p(2, 6), p(6, 2), S, p(4, 0), p(4, 12)))
        object Ampersand : Font(g(p(8, 12), p(4, 0), p(8, 4), p(0, 8), p(4, 12), p(8, 8)))
// object [ : Font(g('[(p(6, 0), p(2, 0), p(2, 12), p(6, 12)))
// object ] : Font(g(p(2, 0), p(6, 0), p(6, 12), p(2, 12)))
// object ( : Font(g((p(6, 0), p(2, 4), p(2, 8), p(6, 12)))
// object ) : Font(g(p(2, 0), p(6, 4), p(6, 8), p(2, 12)))
// object { : Font(g('{(p(6, 0), p(4, 2), p(4, 10), p(6, 12), Separator, p(2, 6), p(4, 6)))
// object } : Font(g(p(4, 0), p(6, 2), p(6, 10), p(4, 12), Separator, p(6, 6), p(8, 6)))
// object % : Font(g(p(0, 0), p(8, 12), Separator, p(2, 10), p(2, 8), Separator, p(6, 4), p(6, 2)))
// object < : Font(g(p(6, 0), p(2, 6), p(6, 12)))
// object > : Font(g(p(2, 0), p(6, 6), p(2, 12)))
// object | : Font(g(p(4, 0), p(4, 5), Separator, p(4, 6), p(4, 12)))
// object : : Font(g(p(4, 9), p(4, 7), Separator, p(4, 5), p(4, 3)))
// object ; : Font(g(p(4, 9), p(4, 7), Separator, p(4, 5), p(1, 2)))
// object " : Font(g(p(2, 10), p(2, 6), Separator, p(6, 10), p(6, 6)))
// object \ : Font(g(p(2, 6), p(6, 10)))
// object ` : Font(g(p(2, 10), p(6, 6)))
// object ~ : Font(g(p(0, 4), p(2, 8), p(6, 4), p(8, 8)))
    }
}