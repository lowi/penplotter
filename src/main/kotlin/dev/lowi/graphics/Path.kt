package dev.lowi.graphics

import com.curiouscreature.kotlin.math.Float2
import dev.lowi.penplotter.SketchOptions

fun <T> concatenate(vararg lists: List<T>): List<T> {
    return listOf(*lists).flatten()
}

fun isPathPoint(element: PathElement): Boolean =
    when (element) {
        is PathElement.Separator -> false
        is PathElement.PathPoint -> true
    }

sealed class PathElement {

    abstract fun scale(scale: Float2): PathElement
    abstract fun translate(t: Float2): PathElement

    data class PathPoint(private val point: Float2) : PathElement() {
        constructor(x: Int, y: Int) : this(Float2(x.toFloat(), y.toFloat()))

        val x: Float
            get() = this.point.x

        val y: Float
            get() = this.point.y

        val xy: FloatArray
            get() = arrayOf(x, y).toFloatArray()

        override fun scale(scale: Float2): PathElement = PathPoint(this.point * scale)

        override fun translate(t: Float2): PathPoint = PathPoint(this.point + t)
    }

    object Separator : PathElement() {
        override fun scale(scale: Float2): PathElement = this
        override fun translate(t: Float2): PathElement = this
    }

}


open class Path(val elements: List<PathElement>, val weight: Float = 1f, val color: Int = -16777216) :
    Iterable<PathElement> {

    companion object {
        fun fromFloat2(weight: Float = 1f, color: Int = -16777216, vararg points: Float2): Path {
            val elements = listOf(*points).map { PathElement.PathPoint(it) }
            return Path(elements, weight, color)
        }

        fun fromPoints(vararg points: PathElement): Path = Path(points.asList())
        fun fromPoints(points: List<PathElement>): Path = Path(points)
        val EMPTY = Path(emptyList())
    }

    override fun iterator(): Iterator<PathElement> = elements.iterator()

    val boundingBox: BoundingBox
        get() {
            val minX: Float = this.elements.filter { isPathPoint(it) }
                .map { element: PathElement ->
                    (element as PathElement.PathPoint).x
                }.minOrNull()!!

            val minY: Float = this.elements.filter { isPathPoint(it) }
                .map { element: PathElement ->
                    (element as PathElement.PathPoint).y
                }.minOrNull()!!

            val maxX: Float = this.elements.filter { isPathPoint(it) }
                .map { element: PathElement ->
                    (element as PathElement.PathPoint).x
                }.maxOrNull()!!

            val maxY: Float = this.elements.filter { isPathPoint(it) }
                .map { element: PathElement ->
                    (element as PathElement.PathPoint).y
                }.maxOrNull()!!

            val topLeft = Float2(minX, minY)
            val bottomRight = Float2(maxX, maxY)

            return BoundingBox(topLeft, bottomRight)
        }

    fun scale(scale: Float2) = fromPoints(
        this.elements.map {
            when (it) {
                is PathElement.Separator -> PathElement.Separator
                is PathElement.PathPoint -> it.scale(scale)
            }
        }
    )

    fun scale(scale: Float) = fromPoints(
        this.elements.map {
            when (it) {
                is PathElement.Separator -> PathElement.Separator
                is PathElement.PathPoint -> it.scale(Float2(scale))
            }
        }
    )

    fun translate(scale: Float2) = fromPoints(
        this.elements.map {
            when (it) {
                is PathElement.Separator -> PathElement.Separator
                is PathElement.PathPoint -> it.translate(scale)
            }
        }
    )

    fun translateX(x: Float) = translate(Float2(x, 0f))
    fun translateY(y: Float) = translate(Float2(0f, y))

    fun translateX(x: Int) = translate(Float2(x.toFloat(), 0f))
    fun translateY(y: Int) = translate(Float2(0f, y.toFloat()))

    inline operator fun plus(other: Path): Path {
        val elements = concatenate(this.elements, listOf(PathElement.Separator), other.elements)
        return Path(elements, this.weight, this.color)
    }
}

interface PathProvider<T> {
    fun generate(options: SketchOptions<T>): List<Path>
}