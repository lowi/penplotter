package dev.lowi.graphics

import com.curiouscreature.kotlin.math.Float2
import com.curiouscreature.kotlin.math.Float4

fun topLeftWidthHeight(topLeft: Float2, width: Float, height: Float): Float4 =
    Float4(topLeft, width, height)

fun topLeftBottomRight(topLeft: Float2, bottomRight: Float2): Float4 {
    val width = bottomRight.x - topLeft.x
    val height = bottomRight.y - topLeft.y
    return Float4(topLeft, width, height)
}

data class BoundingBox(val bb: Float4) {

    constructor(topLeft: Float2, width: Float, height: Float):
            this(topLeftWidthHeight(topLeft, width, height))

    constructor(topLeft: Float2, bottomRight: Float2):
            this(topLeftBottomRight(topLeft, bottomRight))

    override fun toString(): String = "BoundingBox: topLeft: (x=${bb.x}, y=${bb.y}; dimensions: (w=${bb.z}, h=${bb.w})"
}