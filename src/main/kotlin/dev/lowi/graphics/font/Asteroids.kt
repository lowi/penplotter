package dev.lowi.graphics.font

import com.curiouscreature.kotlin.math.Float2
import dev.lowi.graphics.Path
import dev.lowi.graphics.PathElement
import dev.lowi.graphics.PathElement.Separator

// https://github.com/osresearch/vst/blob/master/teensyv/asteroids_font.h
// http://www.edge-online.com/wp-content/uploads/edgeonline/oldfiles/images/feature_article/2009/05/asteroids2.jpg

fun p(x: Int, y: Int): PathElement.PathPoint = PathElement.PathPoint(x, y)



object Asteroids {

    private val characters: Map<Char, Path> = listOf(
        Pair(' ', Path.fromPoints(Separator)),
        Pair('.', Path.fromPoints(p(3, 0), p(4, 0))),
        Pair(',', Path.fromPoints(p(2, 0), p(4, 2))),
        Pair('-', Path.fromPoints(p(2, 6), p(6, 6))),
        Pair('+', Path.fromPoints(p(1, 6), p(7, 6), Separator, p(4, 9), p(4, 3))),
        Pair('!', Path.fromPoints(p(4, 0), p(3, 2), p(5, 2), p(4, 0), Separator, p(4, 4), p(4, 12))),
        Pair('#', Path.fromPoints(p(0, 4), p(8, 4), p(6, 2), p(6, 10), p(8, 8), p(0, 8), p(2, 10), p(2, 2))),
        Pair('^', Path.fromPoints(p(2, 6), p(4, 12), p(6, 6))),
        Pair('=', Path.fromPoints(p(1, 4), p(7, 4), Separator, p(1, 8), p(7, 8))),
        Pair('*', Path.fromPoints(p(0, 0), p(4, 12), p(8, 0), p(0, 8), p(8, 8), p(0, 0))),
        Pair('_', Path.fromPoints(p(0, 0), p(8, 0))),
        Pair('/', Path.fromPoints(p(0, 0), p(8, 12))),
        Pair('\\', Path.fromPoints(p(0, 12), p(8, 0))),
        Pair('@', Path.fromPoints(p(8, 4), p(4, 0), p(0, 4), p(0, 8), p(4, 12), p(8, 8), p(4, 4), p(3, 6))),
        Pair('$', Path.fromPoints(p(6, 2), p(2, 6), p(6, 10), Separator, p(4, 12), p(4, 0))),
        Pair('&', Path.fromPoints(p(8, 0), p(4, 12), p(8, 8), p(0, 4), p(4, 0), p(8, 4))),
        Pair('[', Path.fromPoints(p(6, 0), p(2, 0), p(2, 12), p(6, 12))),
        Pair(']', Path.fromPoints(p(2, 0), p(6, 0), p(6, 12), p(2, 12))),
        Pair('(', Path.fromPoints(p(6, 0), p(2, 4), p(2, 8), p(6, 12))),
        Pair(')', Path.fromPoints(p(2, 0), p(6, 4), p(6, 8), p(2, 12))),
        Pair('{', Path.fromPoints(p(6, 0), p(4, 2), p(4, 10), p(6, 12), Separator, p(2, 6), p(4, 6))),
        Pair('}', Path.fromPoints(p(4, 0), p(6, 2), p(6, 10), p(4, 12), Separator, p(6, 6), p(8, 6))),
        Pair('%', Path.fromPoints(p(0, 0), p(8, 12), Separator, p(2, 10), p(2, 8), Separator, p(6, 4), p(6, 2))),
        Pair('<', Path.fromPoints(p(6, 0), p(2, 6), p(6, 12))),
        Pair('>', Path.fromPoints(p(2, 0), p(6, 6), p(2, 12))),
        Pair('|', Path.fromPoints(p(4, 0), p(4, 5), Separator, p(4, 6), p(4, 12))),
        Pair(':', Path.fromPoints(p(4, 9), p(4, 7), Separator, p(4, 5), p(4, 3))),
        Pair(';', Path.fromPoints(p(4, 9), p(4, 7), Separator, p(4, 5), p(1, 2))),
        Pair('"', Path.fromPoints(p(2, 10), p(2, 6), Separator, p(6, 10), p(6, 6))),
        Pair('\'', Path.fromPoints(p(2, 6), p(6, 10))),
        Pair('`', Path.fromPoints(p(2, 10), p(6, 6))),
        Pair('~', Path.fromPoints(p(0, 4), p(2, 8), p(6, 4), p(8, 8))),
        Pair('?', Path.fromPoints(p(0, 8), p(4, 12), p(8, 8), p(4, 4), Separator, p(4, 1), p(4, 0))),
        Pair('A', Path.fromPoints(p(0, 0), p(0, 8), p(4, 12), p(8, 8), p(8, 0), Separator, p(0, 4), p(8, 4))),
        Pair('B', Path.fromPoints(p(0, 0), p(0, 12), p(4, 12), p(8, 10), p(4, 6), p(8, 2), p(4, 0), p(0, 0))),
        Pair('C', Path.fromPoints(p(8, 0), p(0, 0), p(0, 12), p(8, 12))),
        Pair('D', Path.fromPoints(p(0, 0), p(0, 12), p(4, 12), p(8, 8), p(8, 4), p(4, 0), p(0, 0))),
        Pair('E', Path.fromPoints(p(8, 0), p(0, 0), p(0, 12), p(8, 12), Separator, p(0, 6), p(6, 6))),
        Pair('F', Path.fromPoints(p(0, 0), p(0, 12), p(8, 12), Separator, p(0, 6), p(6, 6))),
        Pair('G', Path.fromPoints(p(6, 6), p(8, 4), p(8, 0), p(0, 0), p(0, 12), p(8, 12))),
        Pair('H', Path.fromPoints(p(0, 0), p(0, 12), Separator, p(0, 6), p(8, 6), Separator, p(8, 12), p(8, 0))),
        Pair('I', Path.fromPoints(p(0, 0), p(8, 0), Separator, p(4, 0), p(4, 12), Separator, p(0, 12), p(8, 12))),
        Pair('J', Path.fromPoints(p(0, 4), p(4, 0), p(8, 0), p(8, 12))),
        Pair('K', Path.fromPoints(p(0, 0), p(0, 12), Separator, p(8, 12), p(0, 6), p(6, 0))),
        Pair('L', Path.fromPoints(p(8, 0), p(0, 0), p(0, 12))),
        Pair('M', Path.fromPoints(p(0, 0), p(0, 12), p(4, 8), p(8, 12), p(8, 0))),
        Pair('N', Path.fromPoints(p(0, 0), p(0, 12), p(8, 0), p(8, 12))),
        Pair('O', Path.fromPoints(p(0, 0), p(0, 12), p(8, 12), p(8, 0), p(0, 0))),
        Pair('P', Path.fromPoints(p(0, 0), p(0, 12), p(8, 12), p(8, 6), p(0, 5))),
        Pair('Q', Path.fromPoints(p(0, 0), p(0, 12), p(8, 12), p(8, 4), p(0, 0), Separator, p(4, 4), p(8, 0))),
        Pair('R', Path.fromPoints(p(0, 0), p(0, 12), p(8, 12), p(8, 6), p(0, 5), Separator, p(4, 5), p(8, 0))),
        Pair('S', Path.fromPoints(p(0, 2), p(2, 0), p(8, 0), p(8, 5), p(0, 7), p(0, 12), p(6, 12), p(8, 10))),
        Pair('T', Path.fromPoints(p(0, 12), p(8, 12), Separator, p(4, 12), p(4, 0))),
        Pair('U', Path.fromPoints(p(0, 12), p(0, 2), p(4, 0), p(8, 2), p(8, 12))),
        Pair('V', Path.fromPoints(p(0, 12), p(4, 0), p(8, 12))),
        Pair('W', Path.fromPoints(p(0, 12), p(2, 0), p(4, 4), p(6, 0), p(8, 12))),
        Pair('X', Path.fromPoints(p(0, 0), p(8, 12), Separator, p(0, 12), p(8, 0))),
        Pair('Y', Path.fromPoints(p(0, 12), p(4, 6), p(8, 12), Separator, p(4, 6), p(4, 0))),
        Pair('Z', Path.fromPoints(p(0, 12), p(8, 12), p(0, 0), p(8, 0), Separator, p(2, 6), p(6, 6)))
    )
        .map { (char, path) -> Pair(char, path.scale(Float2(1f, -1f))) } // flip horizontal
        .map { (char, path) -> Pair(char, path.scale(Float2(1/12f, 1/12f))) } // normalize
        .toMap()

    fun get(char: Char): Path = characters[char]!!
}













































































