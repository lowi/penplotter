package dev.lowi.graphics

import dev.lowi.graphics.Path.Companion.EMPTY
import dev.lowi.graphics.font.Asteroids

object Text {

    fun generate(text: String): Path {
        return text.map { char -> Asteroids.get(char) }
            .reduce { sum, element -> sum + element }
    }
}