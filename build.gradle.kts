import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.10"
}

group = "dev.lowi.penplotter"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven { url = uri("https://clojars.org/repo/") }
//    maven { url = uri("https://dl.bintray.com/korlibs/korlibs") }
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    implementation("org.processing:core:3.3.7")
    implementation("quil:processing-svg:3.5.4")
    implementation("org.apache.xmlgraphics:batik-svggen:1.9.1")
    implementation("org.apache.xmlgraphics:batik-dom:1.7")

//    implementation("com.soywiz.korlibs.korma:korma-jvm:1.9.1")
//    implementation("com.soywiz.korlibs.korma:korma-shape:1.9.1")
    implementation("org.clojars.hozumi:toxiclibscore:0022")

//    testImplementation(kotlin("test-junit"))
}



tasks.test {
    useJUnit()
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "1.8"
}